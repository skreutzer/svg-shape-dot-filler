The PHP code generates shapes filled by dots/pixels from "masks"
(bit pattern for binary and-operation) as SVG being based on
vector outline areas doesn't support the notion of dot/pixel fill.

The JavaScript version does the same, but instead of using a
mask file, the circle and rectangle area are applied per
mathematical calculation instead of supporting arbitrary
shapes (via painting/drawing).
