<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of svg_shape_dot_filler.
 *
 * svg_shape_dot_filler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * svg_shape_dot_filler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with svg_shape_dot_filler. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2023-04-17
 */


require_once("MaskXpmLoader.inc.php");



$cutoutMaskName = "none";

/** @todo This probably comes from elsewhere, is already done elsewhere. */
if (isset($_GET["shape"]) === true)
{
    $cutoutMaskName = $_GET["shape"];
}

// Sanitize (not passing user input directly into paths, etc.)
switch ($cutoutMaskName)
{
    case "none":
        break;
    case "bolt":
        $cutoutMaskName = "bolt";
        break;
    default:
        /** @todo Handle! */
        $cutoutMaskName = "none";
        break;
}

if ($cutoutMaskName != "none")
{
    $fillerSize = 5;

    generateFillerCircle(840,
                         $cutoutMaskName,
                         "filler",
                         $fillerSize,
                         1,
                         3,
                         "filler-svg",
                         dirname(__FILE__)."/filler/".$cutoutMaskName."_".$fillerSize.".svg",
                         true);
}



/**
 * @param[in] $targetSize Dimension of the entire target image, which is a square.
 * @param[in] $fillerSize Dimension of the filler shape. Shape is assumed to be
 *     a square.
 * @param[in] $svgFillerGroupClassName Just used to potentially identify the filler SVG group.
 *     Not per ID, to avoid potential ID uniqueness conflicts. Please provide a valid
 *     CSS class name.
 * @todo Comment on even/odd input parameter arguments.
 */
function generateFillerCircle($targetSize,
                              $cutoutMaskName,
                              $fillerSvg,
                              $fillerSize,
                              $randomizerAcceptanceQuote,
                              $randomizerMax,
                              $svgFillerGroupClassName,
                              $outputFilePath,
                              $debugging)
{
    $mask = null;

    {
        $loader = new MaskXpmLoader();

        if ($loader->loadXpmV3Mask($cutoutMaskName, "\n", "#000000", $mask) != 0)
        {
            return -1;
        }
    }

    $writer = fopen($outputFilePath, "w");

    if ($writer === false)
    {
        return -2;
    }

    if ($debugging === true)
    {
        fwrite($writer, "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"".((int)$targetSize)."\" height=\"".((int)$targetSize)."\">");
    }

    fwrite($writer, "<g class=\"".htmlspecialchars($svgFillerGroupClassName, ENT_XML1 | ENT_QUOTES, "UTF-8")."\">");


    $maskSize = count($mask);
    $maskStep = $maskSize / $targetSize;

    $fillerOffsetCenterX = $fillerSize / 2;
    $fillerOffsetCenterY = $fillerSize / 2;

    for ($y = 0; $y < $targetSize; $y += $fillerSize)
    {
        $maskY = round(($y + 1) * $maskStep);

        if ($maskY < 1 || $maskY > $maskSize)
        {
            continue;
        }

        for ($x = 0; $x < $targetSize; $x += $fillerSize)
        {
            $randomized = rand(0, $randomizerMax);

            if ($randomized > $randomizerAcceptanceQuote)
            {
                continue;
            }

            $maskX = round(($x + 1) * $maskStep);

            if ($maskX < 1 || $maskX > $maskSize)
            {
                continue;
            }

            if ($mask[$maskY][$maskX] == true)
            {
                fwrite($writer, "<rect x=\"".$x."\" y=\"".$y."\" width=\"".((int)$fillerSize)."\" height=\"".((int)$fillerSize)."\" style=\"fill: rgb(0, 0, 0);\" />");
            }
        }
    }

    fwrite($writer, "</g>");

    if ($debugging === true)
    {
        fwrite($writer, "</svg>");
    }

    fclose($writer);
}



?>
