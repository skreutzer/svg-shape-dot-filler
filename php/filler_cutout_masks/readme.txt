So what we want here is an image format which saves black/white (monochrome, binary) pixels.
These pixels are used as a "mask" (as in binary and-operation mask/pattern). So the "black"
(any one of the two binary color options) pixels are used to indicate which parts of an
image are supposed to be filled with filler, and pixels that are not masked and are of the
other color should be skipped. The color does not really matter, and even if there's many
colors, gradients, grayscale, for the mask all that matters and is needed per pixel is
a yes/no, true/false indication which pixels of the mask applied onto SVG should be filled
and which skipped.

This way, creating masks to decide on the pixels for filling can be created by painting the mask.

Unfortunately, there does not seem to be a great choice for binary image formats. There
seems to be no "BPP" (bit-per-pixel) format supported in image editors. ImageMagick supports
to read BMPs, but it's a dependency then to install/configue for PHP via PEAR.

Therefore, XPM seemed to be the easiest option at the moment. XPM is the X PixMap
"format" (= C code) of the X Window System. It can be created/exported with GIMP
and KolourPaint.


 == GIMP ==

Opening an image file (already rasterized like PNG, or SVG that gets translated/imported
as a raster image) in GIMP, you can paint the mask with just black + white colors (maybe
make sure on the color palette that it's actually the precise colors, and not a gradient).
If there's already an SVG outline/frame to work from, GIMP supports handy tools like the
magic lasso, to pick connected surfaces based on a color threshold, so that allows to cut
out or color-fill complex areas/shapes, to eventually replace it all with black + white.
When the drawing is done, there's an item in the menu "Image" -> "Mode" -> "Indexed...",
to select "Use black and white (1-bit) palette" under "Colormap" (and no dithering),
to confirm with "Convert". Then, you can export the result via the menu "File" ->
"Export As...", give the file a name ending with ".xpm", choose the "filler_cutout_masks"
directory as the destination, and choose "X PixMap image | xpm" from the dropdown
"Select File Type (By Extension)", to then confirm with "Export".

 == KolourPaint ==

Similarly, in KolourPaint, after completing the drawing (no need to select mode, as that
only supports grayscale and dithered), you can export via the menu "File" -> "Save As",
give the file a name ending with ".xpm", choose the "filler_cutout_masks"
directory as the destination, choose "XPM image" from the dropdown
"Filter", to then confirm with "Save".


Then the PHP code can load and apply the mask, if the XPM file name (without file name
extension ".xpm") as located in the "filler_cutout_masks" directory is passed to the
respective functions generating the filler from this mask.

The XPM file is just a regular text/code file, which can be opened with a plaintext
editor. Manual manipulation would likely be too inconvenient, in comparison to using
a raster image painting program. On the other hand, it might help to perform
"search & replace" operations in the text editor on unwanted characters. You may also
check the exported color definitions, remove unwanted ones (then don't forget to update
the color definition count given in the first metadata line!) + replace the corresponding
pixel characters, check + make sure that the line ending is in a supported format
("\n" LF for Unix-like, "\r" CR for Mac, but no "\r\n" CRLF for Windows, as this is more
than one character and more effort to match, scan for).
