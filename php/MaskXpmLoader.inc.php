<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of svg_shape_dot_filler.
 *
 * svg_shape_dot_filler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * svg_shape_dot_filler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with svg_shape_dot_filler. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2023-04-16
 */


/**
 * @class MaskXpmLoader
 * @brief For loading a bit mask from XPM image format.
 */

class MaskXpmLoader
{
    /**
    * @brief Reads in a XPM image file and uses one color for identifying pixels to be
    *     used as a mask (as in: binary and-operation mask/pattern).
    * @details For XPM "image" (= C code) format version 3. XPM is the X PixMap format
    *     of the X Window System. This is a class for no other reason than encapsulating
    *     the functions/methods it reuses. Maybe there could eventually be more loaders
    *     for other image formats, or other versions of XPM. No internal state/members
    *     used, method call is "atomic".
    * @param[in] $rowBreakCharacter Line break character, but only a single character supported!
    *     So can be "\n" (LF) for Unix-like, "\r" (CR) for Mac, but not "\r\n" (LFCR) for Windows.
    *     Please make sure the image file was created/exported with a supported line ending format.
    * @param[in] $maskColorHexString The color hex string defined here (like "#000000", uppercase)
    *     will be used to identify the mask pixels, all other colors/pixels defaulting to be non-mask.
    * @param[out] &$mask Two-dimensional array for the XPM image pixels being true or false, indicating
    *     a mask pixel or non-mask pixel.
    */
    public function loadXpmV3Mask($cutoutMaskName, $rowBreakCharacter, $maskColorHexString, &$mask)
    {
        $cutoutMaskFilePath = dirname(__FILE__)."/filler_cutout_masks/".$cutoutMaskName.".xpm";

        if (file_exists($cutoutMaskFilePath) === false)
        {
            /** @retval -1 File as in part specified by $cutoutMaskFilePath seemingly does not exist. */
            return -1;
        }

        $xpmFileContent = @file_get_contents($cutoutMaskFilePath);
        $contentLength = strlen($xpmFileContent);


        $i = 0;

        // Skip preamble (any first lines not starting with a quotation mark).
        for ($i = 0; $i < $contentLength; $i++)
        {
            if ($xpmFileContent[$i] != "\"")
            {
                for (; $i < $contentLength; $i++)
                {
                    if ($xpmFileContent[$i] == $rowBreakCharacter)
                    {
                        break;
                    }
                }
            }
            else
            {
                break;
            }
        }


        $imageWidth = -1;
        $imageHeight = -1;
        $colorDefinitionLines = -1;
        $colorCharacterLength = -1;

        // First line should contain image metadata.
        for (; $i < $contentLength; $i++)
        {
            if ($xpmFileContent[$i] != "\"")
            {
                /** @retval -2 Metadata line didn't start with quotation mark. */
                return -2;
            }

            $firstDataLineStartPos = $i;
            $i++;

            $line = "";

            for (; $i < $contentLength; $i++)
            {
                if ($xpmFileContent[$i] != "\"")
                {
                    // Collecting line characters till end.
                    $line .= $xpmFileContent[$i];
                }
                else
                {
                    {
                        $metadata = $this->extractValues($line);

                        if (count($metadata) < 4)
                        {
                            /** @retval -3 Metadata line didn't contain all the expected fields. */
                            return -3;
                        }

                        $imageWidth = (int)$metadata[0];
                        $imageHeight = (int)$metadata[1];
                        $colorDefinitionLines = (int)$metadata[2];
                        $colorCharacterLength = (int)$metadata[3];

                        if ($colorCharacterLength != 1)
                        {
                            /** @retval -4 No support for color pixel strings with a different length than 1 character. */
                            return -4;
                        }
                    }

                    // Consume everything till + including trailing "row"-/linebreak.
                    for (; $i < $contentLength; $i++)
                    {
                        if ($xpmFileContent[$i] == $rowBreakCharacter)
                        {
                            $i++;
                            break;
                        }
                    }

                    break;
                }
            }

            // Only one line (loop iteration)!
            // The reason to write it as loop anyway is so that
            // I don't have to check with ifs for out-of-bounds
            // $i >= $contentLength on every manual $i++ (as
            // potentially caused by premature end-of-file).
            break;
        }


        $colorCharacterDefinitions = array();

        // Read the color definition lines.
        for ($colorDefinitionLineIndex = 0; $colorDefinitionLineIndex < $colorDefinitionLines; $colorDefinitionLineIndex++)
        {
            for (; $i < $contentLength; $i++)
            {
                if ($xpmFileContent[$i] != "\"")
                {
                    /** @retval -5 Pixels line didn't start with a quotation mark. */
                    return -5;
                }

                $i++;

                $line = "";

                for (; $i < $contentLength; $i++)
                {
                    if ($xpmFileContent[$i] != "\"")
                    {
                        // Collecting line characters till end.
                        $line .= $xpmFileContent[$i];
                    }
                    else
                    {
                        // Consume everything + including trailing "row"-/linebreak.
                        for (; $i < $contentLength; $i++)
                        {
                            if ($xpmFileContent[$i] == $rowBreakCharacter)
                            {
                                $i++;
                                break;
                            }
                        }

                        {
                            $definition = $this->extractValues($line);
                            $definitionIsValid = true;

                            if ($definitionIsValid == true)
                            {
                                if (count($definition) < 3)
                                {
                                    // This line seems to be a broken color definition line.
                                    // However, not returning and trying to continue with
                                    // other lines, as, for example, one image editor (GIMP?)
                                    // created a XPM which used space (!) as the character
                                    // for the color "None", and then didn't use it in the
                                    // image, so let's try to still read the image even
                                    // with such confused/inconvenient color definitions.
                                    $definitionIsValid = false;
                                }
                            }

                            if ($definitionIsValid == true)
                            {
                                if (strlen($definition[0]) != 1)
                                {
                                    // No support for color pixel strings with a different length than 1 character.
                                    $definitionIsValid = false;
                                }
                            }

                            if ($definitionIsValid == true)
                            {
                                if ($definition[1] != "c")
                                {
                                    // Color definition seems to be malformed. No support for
                                    // the other options ("m" monochrome, "g" grayscale, "s" symbolic"),
                                    // only "c" color for now.
                                    $definitionIsValid = false;
                                }
                            }

                            if ($definitionIsValid == true)
                            {
                                $color = strtoupper($definition[2]);

                                $colorValue = null;

                                switch ($color)
                                {
                                case $maskColorHexString:
                                    $colorValue = true;
                                    break;
                                default:
                                    $colorValue = false;
                                    break;
                                }

                                $colorCharacterDefinitions[$definition[0]] = $colorValue;
                            }
                        }

                        break;
                    }
                }

                break;
            }
        }

        {
            // Can also be used to count how many mask/non-mask
            // color characters were defined.

            $hasMaskColorCharacterDefinition = false;

            foreach ($colorCharacterDefinitions as $character => $mask)
            {
                if ($mask == true)
                {
                    $hasMaskColorCharacterDefinition = true;
                    break;
                }
            }

            if ($hasMaskColorCharacterDefinition != true)
            {
                /** @retval -7 Image is missing the color character definition for the
                  * desired mask color $maskColorHexString. */
                return -7;
            }
        }


        $maskColumnSize = -1;

        $mask = array();
        $addRow = true;
        $maskRowIndex = 0;
        $maskColumnIndex = -1;

        for (; $i < $contentLength; $i++)
        {
            if ($xpmFileContent[$i] == "\"")
            {
                $i++;
            }
            else
            {
                // Assuming this is the end, and not bothering
                // with other trailing characters.
                break;
            }

            for (; $i < $contentLength; $i++)
            {
                if ($xpmFileContent[$i] == "\"")
                {
                    $addRow = true;

                    if ($maskColumnSize <= 0)
                    {
                        $maskColumnSize = $maskColumnIndex + 1;

                        if ($maskColumnSize != $imageWidth)
                        {
                            $mask = array();

                            /** @retval -8 Image width as declared in the metadata doesn't
                              * match the pixel line width. $mask remains an empty array. */
                            return -8;
                        }
                    }
                    else
                    {
                        if ($maskColumnSize != $maskColumnIndex + 1)
                        {
                            $mask = array();

                            /** @retval -9 Irregular pixel line width. $mask remains an empty array. */
                            return -9;
                        }
                    }

                    // Consume everything till + including trailing "row"-/linebreak.
                    for (; $i < $contentLength; $i++)
                    {
                        if ($xpmFileContent[$i] == $rowBreakCharacter)
                        {
                            break;
                        }
                    }

                    break;
                }

                if ($addRow == true)
                {
                    $addRow = false;

                    $maskRowIndex++;
                    $mask[$maskRowIndex] = array();
                    $maskColumnIndex = 0;
                }
                else
                {
                    $maskColumnIndex++;
                }

                if (isset($colorCharacterDefinitions[$xpmFileContent[$i]]) == true)
                {
                    $mask[$maskRowIndex][$maskColumnIndex] = $colorCharacterDefinitions[$xpmFileContent[$i]];
                }
                else
                {
                    // Unkown/undefined color! Please check if a "broken" color
                    // definition is used here which we failed/skipped to properly
                    // read in from the color character definitions above?
                    $mask[$maskRowIndex][$maskColumnIndex] = false;
                }
            }
        }

        $maskCount = count($mask);

        if ($maskCount <= 0 ||
            $maskColumnSize <= 0)
        {
            /** @retval -10 Failed to load the mask for an unknown reason. */
            return -10;
        }

        if ($maskCount != $imageHeight)
        {
            /** @retval -11 Image height as declared in the metadata doesn't
              * match the pixel lines count/height. You still get $mask. */
            return -11;
        }

        return 0;
    }

    /**
     * @brief Splits up data as separated by any whitespace characters.
     * @return Array with elements of consecutive data that wasn't whitespace.
     */
    protected function extractValues($line)
    {
        // Can't use explode() because different image editors might
        // put different whitespace in between the values :-(

        $values = array();
        $addValue = true;
        $valuesIndex = -1;

        for ($i = 0, $max = strlen($line); $i < $max; $i++)
        {
            if (ctype_space($line[$i]) !== true)
            {
                if ($addValue == true)
                {
                    $addValue = false;

                    $valuesIndex++;
                    $values[$valuesIndex] = "";
                }

                $values[$valuesIndex] .= $line[$i];
            }
            else
            {
                $addValue = true;

                // Consume whitespace.
                for (; $i < $max; $i++)
                {
                    if (ctype_space($line[$i]) !== true)
                    {
                        // Next iteration of outer loop will increment
                        // $i again, so the index will end up at the first
                        // non-whitespace character.
                        $i--;
                        break;
                    }
                }
            }
        }

        return $values;
    }
}



?>
